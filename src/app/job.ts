export interface job {
    title: string;
    date: Date;
    content: string;
    done: boolean;
}